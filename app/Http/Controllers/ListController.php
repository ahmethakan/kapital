<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ListController extends Controller
{
    public function index()
    {
        $response = Http::get('https://reqres.in/api/users?page=1');

        $data['users'] = $response['data'];

        // return $data['users'];

        return view('responselist', $data);
    }

    public function getdata(Request $request)
    {
        $response = Http::get('https://reqres.in/api/users/'.$request->id);
        $data['user'] = $response;

        return view('userdata', $data);
    }
}
